#![no_std]
#![no_main]

use riscv_rt::entry;

use longan_nano as bsp;

use bsp::hal;
use hal::pac;

use bsp::led::{self, Led};
use hal::{
    gpio::{
        gpioa::{PA11, PA12},
        *,
    },
    prelude::*,
    serial::{Config, Serial, Tx},
};

use synopsys_usb_otg::{UsbBus, UsbPeripheral};
use usb_device::prelude::*;

use core::fmt::Write;
use pac::USART2;
static mut SERIAL_OUT: Option<Tx<USART2>> = None;
macro_rules! serial_write {
    ($($arg:tt)*) => {{
        unsafe {
            match &mut SERIAL_OUT {
                Some(tx) => { write!(tx, $($arg)*) },
                _ => panic!("Serial not initialized!")
            }
        }
    }}
}

static mut EP_MEMORY: [u32; 1024] = [0; 1024];

#[allow(dead_code)]
struct Peripheral {
    pin_dm: PA11<Input<Floating>>,
    pin_dp: PA12<Input<Floating>>,
}

unsafe impl UsbPeripheral for Peripheral {
    const REGISTERS: *const () = 0x50000000 as *const ();

    const HIGH_SPEED: bool = false;
    const FIFO_DEPTH_WORDS: usize = 320;
    const ENDPOINT_COUNT: usize = 4;

    fn enable() {
        let rcu = unsafe { &*pac::RCU::ptr() };

        riscv::interrupt::free(|_| {
            // Enable USB peripheral
            rcu.ahben.modify(|_, w| w.usbfsen().set_bit());

            // Reset USB peripheral
            rcu.ahbrst.modify(|_, w| w.usbfsrst().set_bit());
            rcu.ahbrst.modify(|_, w| w.usbfsrst().clear_bit());
        });
    }

    fn ahb_frequency_hz(&self) -> u32 {
        48_000_000
    }
}

#[entry]
fn main() -> ! {
    let dp = pac::Peripherals::take().unwrap();

    // Configure clocks
    let mut rcu = dp
        .RCU
        .configure()
        .ext_hf_clock(8.mhz())
        .sysclk(96.mhz())
        .freeze();

    assert!(rcu.clocks.usbclk_valid());

    let mut afio = dp.AFIO.constrain(&mut rcu);
    let gpioa = dp.GPIOA.split(&mut rcu);
    let gpiob = dp.GPIOB.split(&mut rcu);
    let gpioc = dp.GPIOC.split(&mut rcu);

    let mut rgb = led::rgb(gpioc.pc13, gpioa.pa1, gpioa.pa2);
    {
        let (r, g, b) = &mut rgb;
        r.off();
        g.off();
        b.off();
    }
    let (_, l, _) = &mut rgb;
    toggle_led(l);

    let (tx, _) = {
        let pin_tx = gpiob.pb10;
        let pin_rx = gpiob.pb11;
        let config = Config::default().baudrate(115_200.bps());
        let serial = Serial::new(dp.USART2, (pin_tx, pin_rx), config, &mut afio, &mut rcu);
        serial.split()
    };
    unsafe { SERIAL_OUT = Some(tx) };
    defmt::info!("RESET");

    let usb = Peripheral {
        pin_dm: gpioa.pa11,
        pin_dp: gpioa.pa12,
    };

    let usb_bus = UsbBus::new(usb, unsafe { &mut EP_MEMORY });

    let mut usb_dev = UsbDeviceBuilder::new(&usb_bus, UsbVidPid(0x16c0, 0x27dd))
        .manufacturer("Fake company")
        .product("Enumeration test")
        .serial_number("TEST")
        .device_class(0x00) // misc
        .build();

    defmt::info!("Entering main loop");
    let mut c: u16 = 0;
    loop {
        if usb_dev.poll(&mut []) {
            defmt::trace!("Main: USB POLL");
        }

        c = c.wrapping_add(1);
        if c == 0 {
            toggle_led(l);
            //defmt::trace!("TOGGLE LED");
        }

        //wfi(); // wait for interrupt
    }
}

#[allow(dead_code)]
#[inline]
fn wfi() {
    unsafe {
        riscv::asm::wfi();
    }
}

fn toggle_led(led: &mut dyn Led) -> () {
    if led.is_on() {
        led.off();
    } else {
        led.on();
    }
}

#[panic_handler]
fn panic_to_serial(info: &core::panic::PanicInfo) -> ! {
    let _ = serial_write!("{}\r\n", info);

    loop {}
}

static mut ENCODER: defmt::Encoder = defmt::Encoder::new();

#[defmt::global_logger]
struct DefmtLogger;

unsafe impl defmt::Logger for DefmtLogger {
    fn acquire() {
        //let _ = serial_write!("DefmtLogger::acquire\r\n");
        //unsafe {
        //    riscv::interrupt::disable();
        //}
        unsafe { ENCODER.start_frame(DefmtLogger::do_write) }
    }

    unsafe fn flush() {
        //let _ = serial_write!("DefmtLogger::flush\r\n");
        // Do nothing

        riscv::interrupt::free(|_cs| {
            if let Some(tx) = &mut SERIAL_OUT {
                //while tx.write(0x0a).is_err() {}
                while tx.flush().is_err() {}
            }
        });
    }

    unsafe fn release() {
        //let _ = serial_write!("DefmtLogger::release\r\n");
        ENCODER.end_frame(DefmtLogger::do_write);

        //riscv::interrupt::enable();
    }

    unsafe fn write(bytes: &[u8]) {
        //let _ = serial_write!("DefmtLogger::write()\r\n");
        ENCODER.write(bytes, DefmtLogger::do_write);
    }
}

impl DefmtLogger {
    fn do_write(bytes: &[u8]) {
        unsafe {
            if let Some(tx) = &mut SERIAL_OUT {
                //let _ = serial_write!("W: {:?}\r\n", bytes);
                for &b in bytes {
                    while tx.write(b).is_err() {
                        while tx.flush().is_err() {}
                    }
                }
                while tx.flush().is_err() {}
            } else {
                panic!("Serial not initialized!");
            }
        }
    }
}
