target extended-remote :3333

# print demangled symbols
set print asm-demangle on

set confirm off

# set backtrace limit to not have infinite backtrace loops
set backtrace limit 32

load

##### BR: panic_handler
#break rust_begin_unwind
#break panic_to_serial
##### BR: main
#break main
##### BR: interrupt_handler for TIMER0_UP
#break TIMER0_UP

#continue

monitor reset run
quit
