#!bash

set -euo pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

: ${BUILD_PATH:="$SCRIPT_DIR/../target/riscv32imac-unknown-none-elf/release"}
: ${BINARY:="$(basename "$SCRIPT_DIR")"}
: ${BINARY_FULL_PATH:="${BUILD_PATH}/${BINARY}"}

BINARY_FULL_PATH="$(realpath -e "${BINARY_FULL_PATH}")"

defmt-print -e "${BINARY_FULL_PATH}"
