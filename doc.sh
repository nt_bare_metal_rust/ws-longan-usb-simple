#!/usr/bin/env bash

set -euxo pipefail

cargo doc --workspace --all-features

cd target/riscv32imac-unknown-none-elf/doc/
exec python3 -m http.server
