#!/usr/bin/env bash

set -euxo pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd "${SCRIPT_DIR}"
exec openocd \
	-f interface/ftdi/minimodule.cfg \
        -c "ftdi device_desc {Dual RS232}" \
	-f gd32vf103.cfg \
	-c "init; reset run; exit"
